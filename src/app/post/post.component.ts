import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  @Input() postName: string;
  @Input() postContent: string;
  @Input() postLoveIts: number;
  @Input() postDate: Date;

  constructor() { }

  ngOnInit(): void {
  }

  // tslint:disable-next-line:typedef
  likePost() {
    this.postLoveIts++;
  }

  // tslint:disable-next-line:typedef
  dislikePost() {
    this.postLoveIts--;
  }
}
